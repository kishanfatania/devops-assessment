package maths;

import java.util.Scanner;

public class Maths {

	public static void main(String[] args) {
	}

	public static int addition(int a, int b) {
		System.out.println("Please enter two numbers you would like to add");
		Scanner in = new Scanner(System.in);
		a = in.nextInt();
		b = in.nextInt();
		int ans = a + b;
		System.out.println("The answer is " + ans);
		return ans;
	}

	public static int subtraction(int a, int b) {
		System.out.println("Please enter two numbers you would like to subtract");
		Scanner in = new Scanner(System.in);
		a = in.nextInt();
		b = in.nextInt();
		int ans = a - b;
		System.out.println("The answer is " + ans);
		return ans;
	}

	public static int division(int a, int b) {
		System.out.println("Please enter two numbers you would like to divide");
		Scanner in = new Scanner(System.in);
		a = in.nextInt();
		b = in.nextInt();
		int ans = a / b;
		System.out.println("The answer is " + ans);
		return ans;

	}

	public static int multiplication(int a, int b) {
		System.out.println("Please enter two numbers you would like to multiply");
		Scanner in = new Scanner(System.in);
		a = in.nextInt();
		b = in.nextInt();
		int ans = a * b;
		System.out.println("The answer is " + ans);
		return ans;

	}

}
