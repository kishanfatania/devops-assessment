package test;

import static org.junit.Assert.*;

import org.junit.Test;

import maths.Maths;

public class MathsTest {

	@Test
	public final void testAddition() {
		assertEquals(8, Maths.addition(3, 5));
		assertEquals(10, Maths.addition(5, 5));
		assertEquals(16, Maths.addition(10, 6));
	}

	@Test
	public final void testSubtraction() {
		assertEquals(3, Maths.subtraction(5, 2));
		assertEquals(8, Maths.subtraction(10, 2));
		assertEquals(7, Maths.subtraction(17, 10));
	}

	@Test
	public final void testDivision() {
		assertEquals(3, Maths.division(6, 2));
		assertEquals(2, Maths.division(10, 5));
		assertEquals(6, Maths.division(6, 1));
	}

	@Test
	public final void testMultiplication() {
		assertEquals(10, Maths.multiplication(5, 2));
		assertEquals(25, Maths.multiplication(5, 5));
		assertEquals(9, Maths.multiplication(3, 3));
	}

}
